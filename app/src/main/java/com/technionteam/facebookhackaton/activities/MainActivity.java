package com.technionteam.facebookhackaton.activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.technionteam.facebookhackaton.R;
import com.technionteam.facebookhackaton.receivers.OutGoingCallReceiver;
import com.technionteam.facebookhackaton.utils.SharedPrefsHelper;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText name,address,allergies,medication,emergency_contact_name,emergency_contact_num;
    Spinner date,blood_type;
    CheckBox organ_donor;
    Button button;

    SharedPrefsHelper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        name = (EditText) findViewById(R.id.user_name);
        address = (EditText) findViewById(R.id.user_address);
        allergies = (EditText) findViewById(R.id.allergies);
        medication = (EditText) findViewById(R.id.medication);
        emergency_contact_name = (EditText) findViewById(R.id.emergency_contact_name);
        emergency_contact_num = (EditText) findViewById(R.id.emergency_contact_num);

        date = (Spinner) findViewById(R.id.date_year);
        blood_type = (Spinner) findViewById(R.id.bloodtype_spinner);

        organ_donor = (CheckBox) findViewById(R.id.checkBox);


        button = (Button) findViewById(R.id.saveButton);
        button.setOnClickListener(this);
        helper = new SharedPrefsHelper(PreferenceManager.getDefaultSharedPreferences(getApplicationContext()));
        loadSavedPreferences();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (!(ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) !=
                            PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS) !=
                            PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(this, Manifest.permission.PROCESS_OUTGOING_CALLS) !=
                            PackageManager.PERMISSION_GRANTED)) {

                requestPermissions(new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION,
                        Manifest.permission.INTERNET,
                        Manifest.permission.SEND_SMS,
                        Manifest.permission.PROCESS_OUTGOING_CALLS
                }, 10);

                return;
            }
        }
    }

    private void loadSavedPreferences(){
        name.setText(helper.getName());
        address.setText(helper.getAddress());
        allergies.setText(helper.getAllergies());
        emergency_contact_name.setText(helper.getEmergency_contact_name());
        emergency_contact_num.setText(helper.getEmergency_contact_number());
        medication.setText(helper.getMedications());

        organ_donor.setChecked(helper.getOrgan_donor());

        helper.setBlood_type(blood_type.getSelectedItem().toString());

    }

    private void savePreferences(){
        helper.setName(name.getText().toString());
        helper.setAddress(address.getText().toString());
        helper.setAllergies(allergies.getText().toString());
        helper.setEmergency_contact_name(emergency_contact_name.getText().toString());
        helper.setEmergency_contact_number(emergency_contact_num.getText().toString());
        helper.setMedications(medication.getText().toString());
        helper.setOrgan_donor(organ_donor.isChecked());
        helper.setBirth_year(date.getSelectedItem().toString());
        helper.setBlood_type(blood_type.getSelectedItem().toString());

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 10:
                if (!(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED))
                {
                    return;
                }
        }
    }

    @Override
    public void onClick(View v){
        savePreferences();
        finish();
    }
}
