package com.technionteam.facebookhackaton.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.telephony.SmsManager;
import android.text.format.DateFormat;

import com.technionteam.facebookhackaton.R;
import com.technionteam.facebookhackaton.utils.SharedPrefsHelper;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class SendSmsActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onStart() {
        super.onStart();
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
            locationManager.requestSingleUpdate(LocationManager.NETWORK_PROVIDER, new LocationListener() {
                        @Override
                        public void onLocationChanged(Location location) {
                            String phoneNum = getIntent().getStringExtra(Intent.EXTRA_PHONE_NUMBER);

                            sendEmergencyMessage(phoneNum, buildEmergencyMessage(location));

                            finish();
                        }

                        @Override
                        public void onStatusChanged(String provider, int status, Bundle extras) {

                        }

                        @Override
                        public void onProviderEnabled(String provider) {

                        }

                        @Override
                        public void onProviderDisabled(String provider) {
                            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivity(intent);
                        }
                    },
                    null);
        }
    }
    private String buildEmergencyMessage2(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String meds = prefs.getString("pref_medications","no meds");
        String alergs = prefs.getString("pref_allergies","no allergies");
        StringBuilder msgSB = new StringBuilder();
        msgSB.append("Medications: "+meds+"\n");
        msgSB.append("Allergies: "+alergs+"\n");
        msgSB.append("Donor: ");
        if(prefs.getBoolean("pref_donor",false)){
            msgSB.append("Yes\n");
        } else {
            msgSB.append("No\n");
        }
        return msgSB.toString();
    }

    private String buildEmergencyMessage(Location currLocation) {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        String user_name = prefs.getString("pref_name","No name");
        String bloodType = prefs.getString("pref_blood_type","No blood type");

        String age = prefs.getString("pref_birth_year","1234");
        Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);

        int ageNum = Integer.parseInt(age);



       //
        ageNum = year - ageNum;
        StringBuilder msgSB = new StringBuilder();
        msgSB.append("Name: "+user_name+"\n");

        msgSB.append("maps.google.com/maps?q=loc:");
        msgSB.append(currLocation.getLatitude());
        msgSB.append(",");
        msgSB.append(currLocation.getLongitude()+"\n");

        msgSB.append("Age: "+ageNum+"\n");

          msgSB.append("Blood type: "+bloodType+"\n");
        msgSB.append("Donor: ");
        if(prefs.getBoolean("pref_donor",false)){
            msgSB.append("Yes\n");
        } else {
            msgSB.append("No\n");
        }
        String meds = prefs.getString("pref_medications","no meds");
        String alergs = prefs.getString("pref_allergies","no allergies");
        msgSB.append("Medications: "+meds+"\n");
        msgSB.append("Allergies: "+alergs);
        return msgSB.toString();
    }

    private void sendEmergencyMessage(String phoneNum, String message) {
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(phoneNum, null, message, null, null);
    }
}
