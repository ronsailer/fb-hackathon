package com.technionteam.facebookhackaton.utils; /**
 * Created by Ron on 01/09/2016.
 */

import android.content.SharedPreferences;
import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class SharedPrefsHelper {
    private SharedPreferences prefs;
    public SharedPrefsHelper(SharedPreferences prefs) {
        this.prefs = prefs;
    }

    public String getBlood_type() {
        return prefs.getString("pref_blood_type", "NO_DATA");
    }

    public void setBlood_type(String blood_type) {
        prefs.edit().putString("pref_blood_type", blood_type).apply();
    }

    public String getName() {
        return prefs.getString("pref_name", "");
    }

    public void setName(String name) {
        prefs.edit().putString("pref_name",name).apply();
    }

    public String getAddress() {
        return prefs.getString("pref_address","");
    }

    public void setAddress(String address) {
        prefs.edit().putString("pref_address",address).apply();
    }

    public String getAllergies() {
        return prefs.getString("pref_allergies","");
    }

    public void setAllergies(String allergies) {
        prefs.edit().putString("pref_allergies", allergies).apply();
    }

    public String getMedications() {
        return prefs.getString("pref_medications","");
    }

    public void setMedications(String medications) {
        prefs.edit().putString("pref_medications",medications).apply();
    }

    public boolean getOrgan_donor() {
        return prefs.getBoolean("pref_donor", false);
    }

    public void setOrgan_donor(boolean organ_donor) {
        prefs.edit().putBoolean("pref_donor", organ_donor).apply();
    }

    public String getMy_phone_number() {
        return prefs.getString("pref_my_phone_number", "");
    }

    public void setMy_phone_number(String my_phone_number) {
        prefs.edit().putString("pref_my_phone_number", my_phone_number).apply();
    }

    public String getEmergency_contact_name() {
        return prefs.getString("pref_emergency_contact_name","");
    }

    public void setEmergency_contact_name(String emergency_contact_name) {
        prefs.edit().putString("pref_emergency_contact_name", emergency_contact_name).apply();
    }

    public String getEmergency_contact_number() {
        return prefs.getString("pref_emergency_phone_number","");
    }

    public void setEmergency_contact_number(String emergency_contact_number) {
        prefs.edit().putString("pref_emergency_phone_number", emergency_contact_number).apply();
    }

    public String getBirth_year() {
        return prefs.getString("pref_birth_year","0000");
    }

    public void setBirth_year(String birthYear) {
        prefs.edit().putString("pref_birth_year", birthYear).apply();
    }
}
