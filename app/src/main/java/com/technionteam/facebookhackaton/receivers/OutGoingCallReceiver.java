package com.technionteam.facebookhackaton.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.technionteam.facebookhackaton.activities.SendSmsActivity;

import java.util.Arrays;
import java.util.HashSet;

public class OutGoingCallReceiver extends BroadcastReceiver {

    private HashSet<String> emergencyNumbers = new HashSet<>(Arrays.asList(
            "100", "101", "102", "0544708840","0546607015","0503377171", "0544663672"
    ));

    @Override
    public void onReceive(Context context, Intent intent) {
        String phoneNum = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
        if (isEmergencyNumber(phoneNum)) {
            Intent intent2 = new Intent(context, SendSmsActivity.class);
            intent2.putExtra(Intent.EXTRA_PHONE_NUMBER, phoneNum);
            intent2.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(intent2);
        }
    }

    private boolean isEmergencyNumber(String phoneNum) {
        return emergencyNumbers.contains(phoneNum);
    }
}
